# Put your own config here!
F_CPU = 8000000UL
MCU = atmega8
AVRDUDE=avrdude
#LDFLAGS += -Wl,--section-start=.text=0xE000	# BOOTLOADER_SUPPORT
#CFLAGS  += -mcall-prologues                     # BOOTLOADER_SUPPORT

load: ethersex.bin all
	$(AVRDUDE) -p m8 -U flash:w:ethersex.hex   
fuse:
	$(AVRDUDE) -p m8 -U lfuse:w:0x24:m


